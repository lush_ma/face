# face

#### 介绍
maya表情插件，所有蒙皮骨骼在FaceJoint下，支持与身体骨骼一起导出fbx骨骼动画到游戏引擎，以纯骨骼绑定达到影视卡通绑定的效果。

#### 使用视频
[https://www.bilibili.com/video/av74697724/](https://www.bilibili.com/video/av74697724/)

#### BUG反馈
[https://afdian.net/group/e268edc45df411eaa59b52540025c377](https://afdian.net/group/e268edc45df411eaa59b52540025c377)

#### 开发教程
[https://www.aboutcg.org/courseDetails/1011/introduce](https://www.aboutcg.org/courseDetails/1011/introduce)
